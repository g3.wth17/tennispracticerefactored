
public class TennisGame2 implements TennisGame
{
    private static final int _ENDOFSET = 4;
	private static final int _FORTY = 3;
	private static final int _THIRTY = 2;
	private static final int _FIFTEEN = 1;
	private static final int _LOVE = 0;
	public int P1point = 0;
    public int P2point = 0;
    
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }
    public String getScore(){
        String literalScore = "";
		if (isTie())
			literalScore = getLiteral(P1point) + "-All";
		if (isDeuce())
			literalScore = "Deuce";
		if (isNormal())
			literalScore = getLiteral(P1point) + "-" + getLiteral(P2point);
		if (isAdvantagedOver(P1point, P2point))
			literalScore = "Advantage player1";
		if (isAdvantagedOver(P2point, P1point))
			literalScore = "Advantage player2";
		if (isWinnerOver(P1point, P2point))
			literalScore = "Win for player1";
		if (isWinnerOver(P2point, P1point))
			literalScore = "Win for player2";
        return literalScore;
    }
	private boolean isNormal() {
		return P2point!=P1point;
	}
	private String getLiteral(int playerPoints) {
		String literalTranslation = "";
		if(playerPoints==_LOVE)
			literalTranslation = "Love";
		if (playerPoints==_FIFTEEN)
			literalTranslation = "Fifteen";
		if (playerPoints==_THIRTY)
			literalTranslation = "Thirty";
		if (playerPoints==_FORTY)
			literalTranslation = "Forty";
		return literalTranslation;
	}
	private boolean isWinnerOver(int firstPlayerPoints, int secondPlayerPoints) {
		return firstPlayerPoints>=_ENDOFSET && secondPlayerPoints>=_LOVE && (firstPlayerPoints-secondPlayerPoints)>=_THIRTY;
	}
	private boolean isAdvantagedOver(int firstPlayerPoints, int secondPlayerPoints) {
		return firstPlayerPoints > secondPlayerPoints && secondPlayerPoints >= _FORTY;
	}
	private boolean isTie() {
		return P1point == P2point && P1point < 4;
	}
	private boolean isDeuce() {
		return P1point==P2point && P1point>=_FORTY;
	}    
    public void SetP1Score(int number){
        for (int i = 0; i < number; i++)
            P1Score();     
    }
    
    public void SetP2Score(int number){
        for (int i = 0; i < number; i++)
            P2Score();    
    }
    
    public void P1Score(){
        P1point++;
    }
    
    public void P2Score(){
        P2point++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}